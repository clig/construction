package mvc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EnterpriseMapper implements RowMapper<Enterprise> {

	@Override
	public Enterprise mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Enterprise enterprise = new Enterprise();
		enterprise.setEnt_id(rs.getString("ent_id"));
		enterprise.setEnt_name(rs.getString("ent_name"));
		enterprise.setBegin_date(rs.getString("begin_date"));
		enterprise.setAddr(rs.getString("addr"));
		enterprise.setReg_capital(rs.getBigDecimal("reg_capital"));
		return enterprise;
	}
	
}
