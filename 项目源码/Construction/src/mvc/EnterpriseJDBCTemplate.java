package mvc;

import java.math.BigDecimal;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

// 定义的 EnterpriseDAO 接口实现类文件
public class EnterpriseJDBCTemplate implements EnterpriseDAO {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	@Override
	public void setDataSource(DataSource dataSource) {
		// TODO Auto-generated method stub
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	@Override
	public void create(String ent_id, String ent_name, String begin_date, String addr, BigDecimal reg_capital) {
		// TODO Auto-generated method stub
		if(begin_date.equals("")) begin_date = null;
		String SQL = "insert into T_enterprise_info (ent_id,ent_name,begin_date,addr,reg_capital) values (?,?,?,?,?)";
		jdbcTemplateObject.update(SQL,ent_id,ent_name,begin_date,addr,reg_capital);
	}

	@Override
	public Enterprise getEnterprise(String id) {
		// TODO Auto-generated method stub
		 String SQL = "select * from T_enterprise_info where ent_id = ?";
		 Enterprise enterprise = jdbcTemplateObject.queryForObject(SQL, 
	                        new Object[]{id}, new EnterpriseMapper());
	     return enterprise;
	}

	@Override
	public List<Enterprise> listEnterprises() {
		// TODO Auto-generated method stub
		String SQL = "select * from T_enterprise_info";
	    List <Enterprise> enterprises = jdbcTemplateObject.query(SQL, 
	                                new EnterpriseMapper());
	    return enterprises;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		String SQL = "delete from T_enterprise_info where ent_id = ?";
	    jdbcTemplateObject.update(SQL, id);
	}

	@Override
	public void update(String ent_id, String ent_name, String begin_date, String addr, BigDecimal reg_capital) {
		// TODO Auto-generated method stub
		if(begin_date.equals("")) begin_date = null;
		String SQL = "update T_enterprise_info set ent_id = ?,ent_name = ?,begin_date = ?,addr = ?,reg_capital = ? where ent_id = ?";
	    jdbcTemplateObject.update(SQL,ent_id,ent_name,begin_date,addr,reg_capital,ent_id);
	}

}
