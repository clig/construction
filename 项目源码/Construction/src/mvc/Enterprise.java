package mvc;

import java.math.BigDecimal;

public class Enterprise {
	private String ent_id;
	private String ent_name;
	private String begin_date;
	private String addr;
	private BigDecimal reg_capital;
	public String getEnt_id() {
		return ent_id;
	}
	public void setEnt_id(String ent_id) {
		this.ent_id = ent_id;
	}
	public String getEnt_name() {
		return ent_name;
	}
	public void setEnt_name(String ent_name) {
		this.ent_name = ent_name;
	}
	public String getBegin_date() {
		return begin_date;
	}
	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public BigDecimal getReg_capital() {
		return reg_capital;
	}
	public void setReg_capital(BigDecimal reg_capital) {
		this.reg_capital = reg_capital;
	}
}
