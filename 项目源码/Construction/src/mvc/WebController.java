package mvc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;




@Controller
public class WebController {
	ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
	EnterpriseJDBCTemplate jdbc = (EnterpriseJDBCTemplate) context.getBean("enterpriseJDBCTemplate");
	@RequestMapping(value="/",method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	@RequestMapping(value="/list",method = RequestMethod.GET)
	public String list(ModelMap m) {
		List<Enterprise> list = jdbc.listEnterprises();
        m.addAttribute("list",list);
		return "list";
	}
	@RequestMapping(value="/add",method = RequestMethod.GET)
	public String add() {
		return "add";
	}
	@RequestMapping(value="/addData",method = RequestMethod.POST)
	public String addData(Enterprise e) {
		jdbc.create(e.getEnt_id(), e.getEnt_name(), e.getBegin_date(), e.getAddr(), e.getReg_capital());
		return "redirect:list";
	}
	@RequestMapping(value="/delete",method = RequestMethod.GET)
	public String delete(String id) {
		jdbc.delete(id);
		return "redirect:list";
	}
	@RequestMapping(value="/edit",method = RequestMethod.GET)
	public String edit(String id,ModelMap m) {
		Enterprise edit = jdbc.getEnterprise(id);
        m.addAttribute("edit",edit);
		return "edit";
	}
	@RequestMapping(value="/updata",method = RequestMethod.POST)
	public String updata(Enterprise e) {
		jdbc.update(e.getEnt_id(), e.getEnt_name(), e.getBegin_date(), e.getAddr(), e.getReg_capital());
		return "redirect:list";
	}
}
