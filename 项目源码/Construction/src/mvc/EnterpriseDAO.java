package mvc;

import java.math.BigDecimal;
import java.util.List;

import javax.sql.DataSource;

//数据访问对象接口文件
public interface EnterpriseDAO {
	// 初始化数据库连接
		public void setDataSource(DataSource ds);
		// 添加企业信息表数据
		public void create(String ent_id,String ent_name,String begin_date,String addr,BigDecimal reg_capital);
		// 获取企业ID
		public Enterprise getEnterprise(String id);
		// 企业列表
		public List<Enterprise> listEnterprises();
		// 删除学生
		public void delete(String id);
		// 修改学生
		public void update(String ent_id,String ent_name,String begin_date,String addr,BigDecimal reg_capital);
}
