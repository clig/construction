<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>建设工程监管信息系统</title>
<link rel="stylesheet" href="resources/bootstrap.min.css">  
<script src="resources/jquery.min.js"></script>
<script src="resources/bootstrap.min.js"></script>
<script src="resources/layer/layer.js"></script>
</head>
<body>
	<div class="jumbotron text-center" style="margin-bottom:0">
		<h3>企业信息修改</h3>
		<form method="post" action="updata">
			<table class="table table-bordered">
	        <caption>•企业信息管理——企业信息修改（以下带<span style="color: red">*</span>为必填项）</caption>
		        <tr>
		            <td>企业编号：<input type="text" name="ent_id" size="45" value="${edit.ent_id}" /><span style="color: red">*</span></td>
		        </tr>
		         <tr>
		            <td>企业名称：<input type="text" name="ent_name" size="45" value="${edit.ent_name}" /><span style="color: red">*</span></td>
		        </tr>
		         <tr>
		            <td>建立时间：<input type="text" name="begin_date" size="45" value="${edit.begin_date}" /></td>
		        </tr>
		         <tr>
		            <td>详细地址：<input type="text" name="addr" size="45" value="${edit.addr}" /></td>
		        </tr>
		         <tr>
		            <td>注册资本：<input type="text" name="reg_capital" size="45" value="${edit.reg_capital}" /></td>
		        </tr>
		     </table>
			<button type="button" class="btn btn-success text-center" onclick="submitAdd();">确定</button>
​		</form>
	</div>
</body>
</html>
<script type="text/javascript">
function submitAdd(){
	//获取input的值
	var projectId = $('[name="ent_id"]').val();
	var projectName = $('[name="ent_name"]').val();
	
	if(projectId == null || projectId == ''){
		layer.alert("企业编号不能为空");
	}else if(projectName == null || projectName == ''){
		layer.alert("企业名称不能为空");
	}else{
		//通过验证，则提交表单
		$("form").submit();
	}
}

</script>