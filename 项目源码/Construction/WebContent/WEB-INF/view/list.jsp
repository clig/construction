<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>建设工程监管信息系统</title>
<link rel="stylesheet" href="resources/bootstrap.min.css">  
<script src="resources/jquery.min.js"></script>
<script src="resources/bootstrap.min.js"></script>
<script src="resources/layer/layer.js"></script>
</head>
<body>
	<div class="jumbotron text-center" style="margin-bottom:0">
		<h3>企业信息列表</h3>
		<button type="button" class="btn btn-success pull-right" onclick="location='add'">增加企业</button>
		<table class="table table-bordered table-hover">
        <caption>•企业信息</caption>
	    <thead>
	        <tr>
	            <th class="text-center">企业编号</th>
	            <th class="text-center">企业名称</th>
	            <th class="text-center">建立时间</th>
	            <th class="text-center">详细地址</th>
	            <th class="text-center">注册资本</th>
	            <th class="text-center" width="10%">相关操作</th>
	        </tr>
	    </thead>
	    <tbody>
	    
	    	<c:forEach var="a" items="${list}">
               <tr>
                  <td>${a.ent_id}</td>
                  <td>${a.ent_name}</td>
                  <td>${a.begin_date}</td>
                  <td>${a.addr}</td>
                  <td>${a.reg_capital} 万元</td>
                  <td><button type="button" class="btn btn-primary" onclick="location='edit?id=${a.ent_id}'">修改</button>&emsp;<button type="button" class="btn btn-warning" onclick="delete_comfirm(${a.ent_id})">删除</button></td>          
                </tr>
             </c:forEach>
	    </tbody>
	</table>
​
	</div>
</body>
</html>
<script>
/**
 *     删除提示方法
 * @param id
 */
function  delete_comfirm(id) {
	layer.confirm("你确定要删除这条信息吗？",()=>{
		window.location.replace("delete?id="+id);
	});
}
</script>