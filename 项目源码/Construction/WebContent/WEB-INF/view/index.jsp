<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>建设工程监管信息系统</title>
<link rel="stylesheet" href="resources/bootstrap.min.css">  
<script src="resources/jquery.min.js"></script>
<script src="resources/bootstrap.min.js"></script>
</head>
<body>
	<div class="jumbotron text-center" style="margin-bottom:0">
		<h1>建设工程监管信息系统首页</h1>
		<button type="button" class="btn btn-info" onclick="location='list'">前往企业信息列表页面</button>
	</div>
</body>
</html>