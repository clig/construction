/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : constructiondb

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 20/09/2020 12:16:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_enterprise_info
-- ----------------------------
DROP TABLE IF EXISTS `t_enterprise_info`;
CREATE TABLE `t_enterprise_info`  (
  `Ent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Ent_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Begin_date` date NULL DEFAULT NULL,
  `Addr` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Reg_capital` decimal(12, 4) NULL DEFAULT NULL,
  PRIMARY KEY (`Ent_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_enterprise_info
-- ----------------------------
INSERT INTO `t_enterprise_info` VALUES ('200200078', '郴职城建有限公司 ', '2020-01-01', '郴职市 ', 1000.0000);
INSERT INTO `t_enterprise_info` VALUES ('202009812', '天信建筑企业有限 \r\n公司\r\n', '2010-02-01', '郴职市', 1500.0000);

SET FOREIGN_KEY_CHECKS = 1;
